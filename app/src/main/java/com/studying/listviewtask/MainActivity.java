package com.studying.listviewtask;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {

    private ListView myListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        myListView = findViewById(R.id.my_list);

        ArrayAdapter arrayAdapter = new ArrayAdapter(this,
                android.R.layout.simple_expandable_list_item_1,
                Phone.myPhones);

        myListView.setAdapter(arrayAdapter);
        myListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Phone itemPhone = Phone.myPhones[position];
                AlertDialog dialog = new AlertDialog.Builder(MainActivity.this)
                        .setTitle(itemPhone.toString())
                        .setMessage(String.format("Cost: %d UAH%nWeight: %sg", itemPhone.getCost(), itemPhone.getWeight()))
                        .setPositiveButton("Close", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .create();
                dialog.show();
            }
        });
    }
}

package com.studying.listviewtask;

import androidx.annotation.NonNull;

public final class Phone {
    private String brand;
    private String model;
    private int cost;
    private int weight;

    public int getCost() {
        return cost;
    }

    public int getWeight() {
        return weight;
    }

    private Phone(String brand, String model, int cost, int weight) {
        this.brand = brand;
        this.model = model;
        this.cost = cost;
        this.weight = weight;
    }

    public final static Phone[] myPhones = {
            new Phone("Apple", "Iphone 10",30560,253),
            new Phone("Xiaomi", "Redmi note 8 pro",6500,296),
            new Phone("OnePlus", "7 Pro",15999,308),
            new Phone("Samsung", "Galaxy A51",6787,290),
            new Phone("Samsung", "Galaxy m31",7499,321)
    };

    @NonNull
    @Override
    public String toString() {
        return brand + " " + model;
    }
}
